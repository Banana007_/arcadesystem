package de.lorenz.arcade.map;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class MapPool {

  private final File worldFolder;
  private final List<Map> mapList;
  private final Random random = new Random();

  protected MapPool(File worldFolder, List<Map> mapList) {
    this.worldFolder = worldFolder;
    this.mapList = mapList;
  }

  protected MapPool(File worldFolder) {
    this(worldFolder, new ArrayList<>());
  }

  public Map getRandomMap() {
    int i = random.nextInt(mapList.size());
    return mapList.get(i);
  }

  public Map getRandomMap(Type type) {
    final List<Map> maps = new ArrayList<>();

    for (Map m : mapList) {
      if (m.getType() == type) {
        maps.add(m);
      }
    }
    int i = random.nextInt(maps.size());
    return maps.get(i);
  }

  public Map getMap(String templateName) {
    for (Map map : mapList) {
      if (map.getTemplateName().equals(templateName)) {
        return map;
      }
    }
    return null;
  }

  public <T extends Map> T getMap(String templateName, Class<T> clazz) {
    for (Map map : mapList) {
      if (map.getTemplateName().equals(templateName)
          && map.getClass().equals(clazz)) {
        return clazz.cast(map);
      }
    }
    throw new NullPointerException("There was no Map with this specific Name");
  }

  public void addMap(Map map) {
    mapList.add(map);
  }

  public void removeMap(Map map) {
    mapList.remove(map);
  }

  public File getWorldFolder() {
    return worldFolder;
  }

  public List<Map> getMapList() {
    return Collections.unmodifiableList(mapList);
  }

  public List<Map> getMapList(Type type) {
    List<Map> maps = new ArrayList<>();
    for (Map map : mapList) {
      if (map.getType() == type) {
        maps.add(map);
      }
    }
    return maps;
  }
}
