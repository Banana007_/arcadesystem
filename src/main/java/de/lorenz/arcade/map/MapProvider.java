package de.lorenz.arcade.map;

import de.lorenz.arcade.system.Module;
import de.lorenz.arcade.util.TitleAPI;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

//FIXME Makes problem with Spigot 1.12+
public class MapProvider {

  private final Module module;
  private final MapPool pool;
  private final Set<Map> activeMaps = new HashSet<>();


  public MapProvider(Module module, MapPool mapPool) {
    this.module = module;
    this.pool = mapPool;
  }

  public World createWorld(Map map) throws FileAlreadyExistsException {
    StringBuilder builder = new StringBuilder();
    builder.append(module.getModuleInfo().getName())
        .append(".").append(map.getTemplateName());

    while (new File(builder.toString()).exists()) {
      builder.append(1);
    }

    try {
      FileUtils.copyDirectory(map.getFile(), new File("./" + builder));
    } catch (IOException e) {
      e.printStackTrace();
    }
    World world = Bukkit.createWorld(new WorldCreator(builder.toString()));
    Map worldMap = map.setWorld(world);

    activeMaps.add(worldMap);
    return world;
  }

  public boolean isActive(Map map) {
    return activeMaps.contains(map);
  }

  public void deleteMap(Map map) throws IOException {
    World main = Bukkit.getWorld("world");
    World world = map.getWorld();

    if (world != null) {
      for (Player player : world.getPlayers()) {
        player.teleport(main.getSpawnLocation());
      }
      Bukkit.unloadWorld(world, false);
      activeMaps.remove(map);
    }

    removeWorldContainer(map);
  }

  private void removeWorldContainer(Map map) throws IOException {
    World world = map.getWorld();

   FileUtils.forceDelete(world.getWorldFolder());
  }

  public Map resetMap(Map map) throws IOException {
    World world = map.getWorld();

    for (Player player : world.getPlayers()) {
      TitleAPI
        .sendTitle(player, 0, 30, 0, "§aEinen Moment§8...",
          "§7Die Welt wird zurückgesetzt");
    }
    deleteMap(map);
    createWorld(map);

    return map;
  }

  public void clearWorlds() {
    Iterator<Map> maps = activeMaps.iterator();
    maps.forEachRemaining(map -> {
      try {
        deleteMap(map);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }

  public MapPool getPool() {
    return pool;
  }
}
