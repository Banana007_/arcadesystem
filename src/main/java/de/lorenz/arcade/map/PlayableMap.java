package de.lorenz.arcade.map;

import java.io.File;
import java.util.Set;
import org.bukkit.Location;

public class PlayableMap extends Map {

  private final String displayname;
  private final Location location;
  private final Set<String> authors;
  private final Type type;


  public PlayableMap(File file,
                     String templateName,
                     String displayName,
                     Location location, Set<String> authors) {
    super(file, templateName);
    this.displayname = displayName;
    this.location = location;
    this.authors = authors;
    this.type = Type.GAME;
  }

  public String getDisplayName() {
    return displayname;
  }

  public Set<String> getAuthors() {
    return authors;
  }

  public Location getLocation() {
    if (location.getWorld() == null) {
      location.setWorld(getWorld());
    }
    return location;
  }

  @Override
  public Type getType() {
    return type;
  }
}
