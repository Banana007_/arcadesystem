package de.lorenz.arcade.map;

import java.io.File;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.World;

public class Map {
  private final String templateName;
  private final File file;
  private final Type type;
  private World world;

  public Map(File file, String templateName) {
    this.templateName = templateName;
    this.file = file;
    this.type = Type.NORMAL;
  }

  public String getTemplateName() {
    return templateName;
  }

  public World getWorld() {
    if (world == null) {
      world = Bukkit.getWorld(templateName);
    }
    return world;
  }

  Map setWorld(World world) {
    this.world = world;
    return this;
  }

  public File getFile() {
    return file;
  }


  public Type getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Map map = (Map) o;
    return templateName.equals(map.templateName) && file.equals(map.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(templateName, file);
  }
}
