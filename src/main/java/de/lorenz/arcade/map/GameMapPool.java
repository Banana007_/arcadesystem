package de.lorenz.arcade.map;


import de.lorenz.arcade.system.Module;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GameMapPool extends MapPool {
  private final Module module;

  public GameMapPool(Module module, List<Map> mapList) {
    super(new File(
        "./plugins/ArcadeGames/" + module.getModuleInfo().getName() + "/maps/"),
      mapList);
    this.module = module;
  }

  public GameMapPool(Module module) {
    this(module, new ArrayList<>());
  }

  public Module getModule() {
    return module;
  }
}
