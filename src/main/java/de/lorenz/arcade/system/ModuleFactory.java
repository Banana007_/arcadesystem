package de.lorenz.arcade.system;

import com.google.common.collect.Sets;
import de.lorenz.arcade.map.GameMapPool;
import de.lorenz.arcade.map.Map;
import de.lorenz.arcade.map.MapProvider;
import de.lorenz.arcade.map.PlayableMap;
import de.lorenz.arcade.util.TitleAPI;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitWorker;
import org.slf4j.LoggerFactory;

public class ModuleFactory {

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ModuleFactory.class);
  private final Set<ModuleInfo> moduleInfoSet = new HashSet<>();
  private final CommandRegistry commandRegistry;
  private final ListenerRegistry listenerRegistry;
  private final Logger logger = LogManager.getLogger();
  private Module activeModule;
  private URLClassLoader urlClassLoader;
  private ArcadeMaster arcadeMaster;


  ModuleFactory(CommandRegistry commandRegistry, ListenerRegistry listenerRegistry,
                ArcadeMaster arcadeMaster) {
    this.commandRegistry = commandRegistry;
    this.listenerRegistry = listenerRegistry;
    this.arcadeMaster = arcadeMaster;
  }

  boolean saveModuleInfo(ModuleInfo moduleInfo) {

    ModuleInfo info = containsDups(moduleInfo);
    if (info != null) {
      LOGGER.warn("Module {} at {} was denied, because it has duplicated entries with {} at {}",
          moduleInfo.getName(), moduleInfo.getJarPath(), info.getName(), info.getJarPath());
      return false;
    }

    this.moduleInfoSet.add(moduleInfo);
    return true;
  }

  ModuleInfo containsDups(ModuleInfo moduleInfo) {
    for (ModuleInfo infos : moduleInfoSet) {
      if (infos.getName().equalsIgnoreCase(moduleInfo.getName())) {
        return infos;
      }
      if (infos.getGameMasterPath().equals(moduleInfo.getGameMasterPath())) {
        return infos;
      }
    }
    return null;
  }

  public Module getActiveModule() {
    return activeModule;
  }

  public Set<ModuleInfo> getModuleInfoSet() {
    return Collections.unmodifiableSet(moduleInfoSet);
  }

  public void standBy() {
    if (activeModule != null) {
      disableActiveModule();
    }
  }

  public ModuleInfo getModuleInfoByName(String name) {
    Iterator<ModuleInfo> moduleInfoIterator = moduleInfoSet.iterator();

    while (moduleInfoIterator.hasNext()) {
      ModuleInfo moduleInfo = moduleInfoIterator.next();

      if (moduleInfo.getName().equals(name)) {
        return moduleInfo;
      }
    }
    return null;
  }

  void disableActiveModule() {

    if (this.activeModule == null) {
      return;
    }

    boolean hasStopMethod = true;
    Method method = null;
    try {
      Class<?> classToLoad =
          Class.forName(activeModule.getModuleInfo().getGameMasterPath(),
            true,
            urlClassLoader
          );
      method = classToLoad.getDeclaredMethod("onStop");
    } catch (ClassNotFoundException | NoSuchMethodException ignored) {
      hasStopMethod = false;
    }

    if (hasStopMethod) {
      try {
        method.invoke(activeModule);
      } catch (IllegalAccessException | InvocationTargetException e) {
        logger.warn("An error occurred while disable the current module!", e);
      }
    }

    this.commandRegistry.unloadCommands();
    this.listenerRegistry.unloadListeners();

    try {
      this.urlClassLoader.close();

      Bukkit.getScheduler().cancelTasks(arcadeMaster);
      for (BukkitWorker task : Bukkit.getScheduler().getActiveWorkers()) {
        try { //TODO: replace
          task.getThread().stop();
        } catch (ThreadDeath e) {
          LOGGER.warn("An error occurred while stop the thread", e);
        }
      }
      LOGGER
          .info("Module {} was successfully unloaded", this.activeModule.getModuleInfo().getName());
      this.activeModule.getMapProvider().clearWorlds();
      this.activeModule = null;
    } catch (IOException e) {
      LOGGER.error("An error orrcured while disable the current module!", e);
    }
  }

  public ModuleInfo getActiveModuleInfo() {
    return activeModule != null ? activeModule.getModuleInfo() : null;
  }


  public void reload() {
    disableActiveModule();

    moduleInfoSet.clear();
    loadModuleInfos();
  }

  public void animate(ModuleInfo moduleInfo) {
    char[] chars = moduleInfo.getName().toCharArray();

    Bukkit.getScheduler().runTask(arcadeMaster, () -> {
      StringBuilder builder = new StringBuilder(chars.length);
      for (int i = 0; i < chars.length; i++) {
        String pref = builder.toString();
        builder.append("§a").append(chars[i]);

        for (Player all : Bukkit.getOnlinePlayers()) {
          TitleAPI.sendTitle(all, 0, 20, 0, pref + "§e" + chars[i], "");
        }
        try {
          Thread.sleep(200);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      for (Player all : Bukkit.getOnlinePlayers()) {
        TitleAPI.sendTitle(all, 0, 20, 20, builder.toString(), "");
      }
    });
  }

  public void loadModuleInfos() {
    try {
      File file = new File("./plugins/ArcadeGames/");
      if (!file.exists()) {
        file.mkdirs();
        return;
      }

      if (file.list() == null) {
        return;
      }

      for (String files : file.list((dir, name) -> name.endsWith(".jar"))) {
        ModuleInfo moduleInfo = loadModuleInfo(files);
        if (moduleInfo != null) {
          LOGGER.info("The Module {} version {} by {} was injected",
            moduleInfo.getName(),
            moduleInfo.getVersion(),
            String.join(" , ", moduleInfo.getAuthors())
          );
        } else {
          arcadeMaster.getLogger()
              .log(Level.WARNING, "There is an error occurred while loading Module {0}", files);
        }
      }
    } catch (SecurityException ex) {
      arcadeMaster.getLogger().log(Level.SEVERE, "No read access to the directory!", ex);
    }
  }

  /**
   * Active a {@link Module}, when a {@link Module} already activated, the current active {@link Module}
   * will be disabled
   *
   * @param moduleInfo from the Module
   * @return If the Module successful started
   */
  public boolean renderModule(ModuleInfo moduleInfo, Properties properties) {
    if (this.activeModule != null) {
      disableActiveModule();
    }

    if (moduleInfo == null) {
      return false;
    }

    File file = new File(moduleInfo.getJarPath());
    if (!file.exists()) {
      return false;
    }
    try {
      this.urlClassLoader = new URLClassLoader(
          new URL[] {file.toURI().toURL()},
          this.getClass().getClassLoader()
      );

      Class<?> classToLoad = Class.forName(moduleInfo.getGameMasterPath(), true, urlClassLoader);

      Method method;
      //TODO: remove 1.7
      try {
        method = classToLoad.getDeclaredMethod("onStart", Properties.class);
      } catch (NoSuchMethodException e) {
        method = classToLoad.getDeclaredMethod("onStart");
      }
      Module instance = (Module) classToLoad.newInstance();

      this.activeModule = instance;
      this.activeModule.setModuleInfo(moduleInfo);
      GameMapPool pool = fillMapPool(instance, moduleInfo);
      this.activeModule.setMapProvider(new MapProvider(instance, pool));
      method.invoke(instance, properties);

      animate(moduleInfo);
      return true;
    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | MalformedURLException | IllegalAccessException | ClassNotFoundException e) {
      logger.warn("There is an error occurred while enable the Module {}", moduleInfo.getName(), e);
    } catch (IOException e) {
      logger.warn("There is an error occurred while fill the Module Pool {}", moduleInfo.getName(), e);
    }
    return false;
  }

  public boolean renderModule(ModuleInfo moduleInfo) {
    return renderModule(moduleInfo, new Properties());
  }

  private GameMapPool fillMapPool(Module module, ModuleInfo info) throws IOException {
    File file = new File("./plugins/ArcadeGames/" + info.getName() + "/maps/");
    if (!file.exists()) {
      file.mkdirs();
      return new GameMapPool(module);
    }

    List<Map> mapList = new ArrayList<>();
    for (File worlds : file.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
      File mapFile = new File(worlds.getAbsolutePath() + "/map.yml");
      Map generatedMap;

      if (FileUtils.directoryContains(worlds, mapFile)) {
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(mapFile);

        String displayname = configuration.getString("displayname");
        Set<String> authors = Sets.newHashSet(configuration.getStringList("authors"));

        double x = configuration.getDouble("spawn.x");
        double y = configuration.getDouble("spawn.y");
        double z = configuration.getDouble("spawn.z");
        float yaw = (float) configuration.getDouble("spawn.yaw");
        float pitch = (float) configuration.getDouble("spawn.pitch");
        generatedMap = new PlayableMap(worlds, worlds.getName(), displayname,
            new Location(null, x, y, z, yaw, pitch), authors);
      } else {
        generatedMap = new Map(worlds, worlds.getName());
      }

      mapList.add(generatedMap);
      logger.info("Add Map {} at {} for Module {}", worlds.getName(), worlds.getPath(),
          module.getModuleInfo().getName());
    }
    return new GameMapPool(module, mapList);
  }

  /**
   * Add a {@link Module} to the system.
   * It is important to note that not the module itself is loaded,
   * but only the {@link ModuleInfo}, which is later important for module rendering.
   *
   * @param name from the jar file
   * @return The {@link ModuleInfo} or null if the module could not be loaded.
   */
  ModuleInfo loadModuleInfo(String name) {
    logger.info("Try to load Module {}", name);
    File jarFile = new File("./plugins/ArcadeGames/" + name);
    try (URLClassLoader classLoader = new URLClassLoader(
        new URL[] {jarFile.toURI().toURL()},
        this.getClass().getClassLoader())) {
      InputStream gameFileInput = classLoader.getResourceAsStream("game.yml");

      if (gameFileInput == null) {
        logger.error("There is no required game.yml file in {}", name, new NoSuchElementException());
        return null;
      }

      Reader gameYamlReader = new InputStreamReader(gameFileInput);
      YamlConfiguration configuration = YamlConfiguration.loadConfiguration(gameYamlReader);
      UUID moduleId = UUID.randomUUID();
      String main = configuration.getString("main");

      if (main == null) {
        logger.error("game.yml does not contains a valid main path!", new NoSuchElementException());
        return null;
      }

      ModuleInfo moduleInfo = new ModuleInfo() {
        @Override
        public UUID getUUID() {
          return moduleId;
        }

        @Override
        public String getJarPath() {
          return "./plugins/ArcadeGames/" + name;
        }

        @Override
        public String getGameMasterPath() {
          return main;
        }

        @Override
        public String getName() {
          String name = configuration.getString("name");
          return name != null ? name.replace(' ', '_') : this.getUUID().toString();
        }

        @Override
        public String[] getAuthors() {
          if (configuration.getString("author") != null) {
            return new String[] {configuration.getString("author")};
          }
          List<String> configAuthors = configuration.getStringList("authors");
          if (configAuthors == null) {
            return new String[0];
          }

          String[] authors = new String[configAuthors.size()];
          return configAuthors.toArray(authors);
        }

        @Override
        public String getVersion() {
          String version = configuration.getString("version");
          return version != null ? version : "N/A";
        }

        @Override
        public Material getDisplayMaterial() {
          String material = configuration.getString("displayMaterial");
          return material != null ? Material.matchMaterial(material) : Material.STONE;
        }

        @Override
        public boolean isHidden() {
          return configuration.getBoolean("hidden");
        }
      };

      boolean saved = saveModuleInfo(moduleInfo);
      if (!saved) {
        logger.warn(
            "There is an error occurred by try to save the ModuleInfo in the ModuleRender");
      }
      return saved ? moduleInfo : null;

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public String toString() {
    return "ModuleRender{" +
        "moduleInfoSet=" + moduleInfoSet +
        ", activeModule=" + activeModule +
        ", commandRegistry=" + commandRegistry +
        ", listenerRegistry=" + listenerRegistry +
        '}';
  }
}
