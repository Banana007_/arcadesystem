package de.lorenz.arcade.system;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class ListenerRegistry {

    private final ArcadeMaster master;
    private final Set<Listener> listeners = new HashSet<>();


    ListenerRegistry(ArcadeMaster master) {
        this.master = master;
    }

    public void addListenerEntry(Listener listener) {
        this.listeners.add(listener);
        Bukkit.getPluginManager().registerEvents(listener, master);
    }

    void unloadListeners() {
        for (Listener l : listeners) {
            HandlerList.unregisterAll(l);
            master.getLogger().log(Level.FINE, "Listener {0} was unloaded by the Master!", l);
        }
        listeners.clear();
    }
}
