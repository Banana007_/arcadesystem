package de.lorenz.arcade.system;

public enum Status {
  PLAYING,
  READY,
  OFFLINE
}
