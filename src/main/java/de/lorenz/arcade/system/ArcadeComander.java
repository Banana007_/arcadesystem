package de.lorenz.arcade.system;

import org.bukkit.command.Command;
import org.bukkit.event.Listener;

public class ArcadeComander {

    private CommandRegistry commandRegistry;
    private ListenerRegistry listenerRegistry;

    private ArcadeComander() { }

    ArcadeComander(CommandRegistry commandRegistry, ListenerRegistry listenerRegistry) {
        this.commandRegistry = commandRegistry;
        this.listenerRegistry = listenerRegistry;
    }

    /**
     * Loads a command and binds it to the currently
     * running game instance. This command is automatically unloaded when the game is changed.
     *
     * @param arcadeCommand The Command class
     */
    public void addCommand(Command arcadeCommand) {
        this.commandRegistry.registerCommand(arcadeCommand);
    }

    /**
     * Loads a listener and binds it to the currently running game instance.
     * This listener is automatically unloaded when the game is changed.
     *
     * @param listener A new {@link Listener} instance.
     */
    public void addListener(Listener listener) {
        this.listenerRegistry.addListenerEntry(listener);
    }
}
