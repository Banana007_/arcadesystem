package de.lorenz.arcade.system;

import java.util.UUID;
import org.bukkit.Material;

/**
 * Contains Information of a {@link Module}.
 */
public interface ModuleInfo {

    /**
     * Get the UUID of the current Game.
     * The UUID change every Session.
     * @return current UUID
     */
    UUID getUUID();

    /**
     * Get the Path of the located Jar file.
     * @return The jar file path.
     */
    String getJarPath();

    /**
     * Get the Path of the Main class who extends {@link Module}
     * @return the main class path
     */
    String getGameMasterPath();

    /**
     * Get the Name of the current {@link Module}.
     * @return the Name
     */
    String getName();

    /**
     * Get the Authors of the Module.
     * @return the authors.
     */
    String[] getAuthors();

    /**
     * Get the Version of the Module.
     * @return the version.
     */

    String getVersion();

    /**
     * Get the {@link Material} of the module.
     * @return the {@link Material}
     */
    Material getDisplayMaterial();

    /**
     * Check if the {@link Module} is hidden, if the {@link Module} is hidden,
     * only players with permission or the {@link #getAuthors()} himself, are allowed to see the {@link Module}.
     * @return If the Module is hidden.
     */
    boolean isHidden();
}
