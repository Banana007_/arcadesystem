package de.lorenz.arcade.system;

import de.lorenz.arcade.map.MapProvider;
import java.util.Properties;
import org.bukkit.command.Command;
import org.bukkit.event.Listener;

public abstract class Module {

    private ModuleInfo moduleInfo;
    private final ArcadeMaster master = ArcadeMaster.getInstance();
    private MapProvider mapProvider;

    protected Module() {}

    /**
     * @deprecated See {@link #onStart(Properties)}
     *
     * This method will be called by the {@link ModuleFactory}, when the game will be activated.
     */
    public void onStart() { }

    /**
     * This method will be called by the {@link ModuleFactory}, when the game will be activated.
     *
     * @param startProperties Get the startProperties of the Module which are transmitted during the command call.
     */
    public void onStart(Properties startProperties) {}

    /**
     * This method will be called by the {@link ModuleFactory}, when the game switched to another or the server is stopped.
     */
    public void onStop() { }

    /**
     * This method will stop the current game.
     */
    public void stop() {
        master.getModuleRender().standBy();
    }

    /**
     * Get the {@link MapProvider} wich is linked by the current game.
     *
     * @return A {@link MapProvider} instance
     */
    public MapProvider getMapProvider() {
        return mapProvider;
    }

    public void registerCommand(Command command) {
        this.master.getCommandRegistry().registerCommand(command);
    }

    public void registerListener(Listener listener) {
        this.master.getListenerRegistry().addListenerEntry(listener);
    }

    /**
     * Get the {@link ModuleInfo} of the current Module.
     * @return the current {@link ModuleInfo}
     */
    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    @Deprecated
    public ArcadeMaster getMaster() {
        return ArcadeMaster.getInstance();
    }

    void setMapProvider(MapProvider mapProvider) {this.mapProvider = mapProvider; }
    void setModuleInfo(ModuleInfo moduleInfo) {
        this.moduleInfo = moduleInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        final Module module = (Module) obj;
        return module.getModuleInfo().getUUID().equals(getModuleInfo().getUUID());
    }
}
