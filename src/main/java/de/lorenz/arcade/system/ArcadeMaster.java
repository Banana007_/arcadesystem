package de.lorenz.arcade.system;

import de.lorenz.arcade.commands.ArcadeCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ArcadeMaster assembles the heart of Arcade and is among other things
 * the interface to JavaPlugin. Since the maste
 * works a lot internally, the accesses are limited.
 */
public class ArcadeMaster extends JavaPlugin {

  private CommandRegistry commandRegistry;
  private ListenerRegistry listenerRegistry;

  private ModuleFactory moduleFactory;
  private static ArcadeMaster instance;
  public static final String PREFIX = "§8» §5Arcade §8┃ §7";
  private Logger logger = LoggerFactory.getLogger(ArcadeMaster.class);


  @Override
  public void onEnable() {
    instance = this; //TODO: away with that
    logger.info("   ('-.     _  .-')              ('-.     _ .-') _     ('-.                  ('-.     _   .-')       ('-.    .-')   ");
    logger.info("  ( OO ).-.( \\( -O )            ( OO ).-.( (  OO) )  _(  OO)                ( OO ).-.( '.( OO )_   _(  OO)  ( OO ). ");
    logger.info("  / . --. / ,------.   .-----.  / . --. / \\     .'_ (,------.   ,----.      / . --. / ,--.   ,--.)(,------.(_)---\\_)");
    logger.info("  | \\-.  \\  |   /`. ' '  .--./  | \\-.  \\  ,`'--..._) |  .---'  '  .-./-')   | \\-.  \\  |   `.'   |  |  .---'/    _ | ");
    logger.info(".-'-'  |  | |  /  | | |  |('-..-'-'  |  | |  |  \\  ' |  |      |  |_( O- ).-'-'  |  | |         |  |  |    \\  :` `. ");
    logger.info(" \\| |_.'  | |  |_.' |/_) |OO  )\\| |_.'  | |  |   ' |(|  '--.   |  | .--, \\ \\| |_.'  | |  |'.'|  | (|  '--.  '..`''.)");
    logger.info("  |  .-.  | |  .  '.'||  |`-'|  |  .-.  | |  |   / : |  .--'  (|  | '. (_/  |  .-.  | |  |   |  |  |  .--' .-._)   \\");
    logger.info("  |  | |  | |  |\\  \\(_'  '--'\\  |  | |  | |  '--'  / |  `---.  |  '--'  |   |  | |  | |  |   |  |  |  `---.\\       /");
    logger.info("  `--' `--' `--' '--'  `-----'  `--' `--' `-------'  `------'   `------'    `--' `--' `--'   `--'  `------' `-----' ");

    // Initial System relevant Methods
    this.commandRegistry = new CommandRegistry(this);
    this.listenerRegistry = new ListenerRegistry(this);
    this.moduleFactory = new ModuleFactory(this.commandRegistry, this.listenerRegistry, this);
    this.moduleFactory.loadModuleInfos();

    ///// Register Master Commands
    getCommand("arcade").setExecutor(new ArcadeCommand(this.moduleFactory, this));
  }

  @Override
  public void onDisable() {
    if (this.moduleFactory.getActiveModule() != null) {
      this.moduleFactory.getActiveModule().getMapProvider().clearWorlds();
    }
  }

  //TODO Object oriented
  public static ArcadeMaster getInstance() {
    return instance;
  }

  ModuleFactory getModuleRender() {
    return this.moduleFactory;
  }

  public CommandRegistry getCommandRegistry() {
    return commandRegistry;
  }

  public ListenerRegistry getListenerRegistry() {
    return listenerRegistry;
  }
}
