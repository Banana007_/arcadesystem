package de.lorenz.arcade.system;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.Map;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;

public class CommandRegistry {

    private List<Command> commands = new ArrayList<>();
    private ArcadeMaster master;

    CommandRegistry(ArcadeMaster master) {
        this.master = master;
    }

    public void registerCommand(Command command) {
        try {
            Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register(command.getName(), command);

            commands.add(command);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    void unloadCommand() {
        try {
            PluginManager manager = master.getServer().getPluginManager();

            Field commandMapField = SimplePluginManager.class.getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            CommandMap map = (CommandMap) commandMapField.get(manager);

            Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
            knownCommandsField.setAccessible(true);
            Map<String, Command> knownCommands = (Map<String, Command>) knownCommandsField.get(map);

            commands.forEach(command -> {
                knownCommands.remove(command.getName().toLowerCase());
                Bukkit.getLogger().log(Level.INFO, "The command {0} was unloaded by the Master.", command.getName());
            });
            commands.clear();
        } catch(NoSuchFieldException |
                IllegalAccessException e) {
            Bukkit.getLogger().log(Level.WARNING, "Command can't unloaded!", e);
        }
    }

    public List<Command> getCommands() {
        return Collections.unmodifiableList(commands);
    }

    void unloadCommands() {
        unloadCommand();
    }
}
