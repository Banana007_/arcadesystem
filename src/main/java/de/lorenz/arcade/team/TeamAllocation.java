package de.lorenz.arcade.team;

import org.bukkit.entity.Player;

public abstract class TeamAllocation {

  abstract void onJoin(Player player);
  abstract void onLeave(Player player);

  public boolean isFull() {
    return true;
  }

  public boolean isOvercrowded() {
    return true;
  }
}
