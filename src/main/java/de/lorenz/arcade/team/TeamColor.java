package de.lorenz.arcade.team;

public enum TeamColor {
  RED,
  GREEN,
  BLUE,
  BROWN,
  PINK,
  PURPLE,
  CYAN,
  YELLOW,
  LIGHT_BLUE,
  GRAY

}
