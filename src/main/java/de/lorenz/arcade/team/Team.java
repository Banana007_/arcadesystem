package de.lorenz.arcade.team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bukkit.entity.Player;

public class Team {

  private List<Player> players = new ArrayList<>();
  private final TeamColor teamColor;


  public Team(TeamColor teamColor) {
    this.teamColor = teamColor;
  }

  public void addPlayer(Player player) {
    players.add(player);
  }

  public void removePlayer(Player player) {
    players.remove(player);
  }

  public List<Player> getPlayers() {
    return Collections.unmodifiableList(players);
  }

  public TeamColor getTeamColor() {
    return teamColor;
  }
}
