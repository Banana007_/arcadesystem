package de.lorenz.arcade.commands;

import de.lorenz.arcade.gui.Pager;
import de.lorenz.arcade.system.ArcadeMaster;
import de.lorenz.arcade.system.ModuleInfo;
import de.lorenz.arcade.system.ModuleFactory;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ArcadeCommand implements CommandExecutor {

  private final ModuleFactory moduleFactory;
  private final ArcadeMaster arcadeMaster;

  private final Pager<String> pager = new Pager<>(
      "§6/arcade play §8[§6§aSpiel§8] §7Lädt ein Spiel§8.",
      "§6/arcade stop §7Stoppt das aktuell laufende Spiel§8.",
      "§6/arcade games §7Zeigt alle Spiele an§8.",
      "§6/arcade map §7Zeigt dir alle Map Befehle an§8.",
      "§6/arcade info §7Zeigt die Informationen zum aktuellen Spiel an§8.",
      "§6/arcade reload §7Lädt alle Spiele neu§8.",
      "§6/arcade help §7Zeigt die aktuelle Übersicht der Befehle an§8."

  );

  public ArcadeCommand(ModuleFactory moduleFactory, ArcadeMaster arcadeMaster) {
    this.moduleFactory = moduleFactory;
    this.arcadeMaster = arcadeMaster;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
    if (args.length == 0
        || (args.length == 1 && args[0].equalsIgnoreCase("help"))
        || (args.length == 2 && args[0].equalsIgnoreCase("help"))) {
      int page = 1;

      if (args.length == 2) {
        try {
          page = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
          page = 1;
        }
      }

      sender.sendMessage(
          "§aÜbersicht der Befehle von Arcade §7(§e" + page + "§8/§e"
            + pager.getPages()
            + "§7)§8:");
      for (String i : pager.getPage(page)) {
        sender.sendMessage(i);
      }
      return true;
    }

    if (args[0].equalsIgnoreCase("map")) {
      if (moduleFactory.getActiveModule() == null) {
        sender.sendMessage(ArcadeMaster.PREFIX + "§cEs läuft derzeit kein Spiel§8!");
        return false;
      }
      MapCommand mapCommand = new MapCommand(moduleFactory.getActiveModule().getMapProvider());
      return mapCommand.onCommand(sender, Arrays.copyOfRange(args, 1, args.length));
    }

    if (args[0].equalsIgnoreCase("reload")) {
      sender.sendMessage(ArcadeMaster.PREFIX + "§aReloading§8...");
      this.moduleFactory.reload();
      sender.sendMessage(ArcadeMaster.PREFIX + "§aReload completed§8!");
    }

    if (args[0].equalsIgnoreCase("games")) {
      final StringBuilder builder = new StringBuilder();
      final Iterator<ModuleInfo> moduleInfoIterator = moduleFactory.getModuleInfoSet().iterator();
      final boolean showHidden = sender.hasPermission("arcade.system.see_hidden");

      while (moduleInfoIterator.hasNext()) {
        ModuleInfo moduleInfo = moduleInfoIterator.next();

        if (moduleInfo.isHidden() && !showHidden) {
          continue;
        }
        builder.append("§a").append(moduleInfo.getName());

        if (moduleInfoIterator.hasNext()) {
          builder.append("§8, ");
        } else {
          builder.append("§8.");
        }
      }

      if (builder.length() == 0) {
        sender.sendMessage(ArcadeMaster.PREFIX + "§cEs wurde kein Spiel gefunden§8.");
        return true;
      }

      sender.sendMessage(
          ArcadeMaster.PREFIX + "§7Aktuelle Spiele §7(§e" + moduleFactory.getModuleInfoSet().size()
            + "§7)§8: " + builder);
      return true;
    }


    if (args[0].equalsIgnoreCase("play")) {
      if (args.length == 1) {
        sender.sendMessage(ArcadeMaster.PREFIX + "§6/arcade play §8[§6§aSpiel§8]");
        return true;
      }
      Properties properties = new Properties();

      if (args.length > 2) {
        for (int i = 2; i < args.length; i++) {
          String arg = args[i];
          String[] prop = arg.split("=");
          if (prop.length == 2) {
            properties.put(prop[0], prop[1]);
          }
        }
      }

      String name = args[1];
      Bukkit.getScheduler().runTask(arcadeMaster, () -> {
        ModuleInfo info = moduleFactory.getModuleInfoByName(name);
        boolean successfully = moduleFactory.renderModule(info, properties);

        //Send message
        String msg = successfully
            ? "§aDas Spiel §5" + info.getName() + " §e" + info.getVersion()
            + " §aby §3"
            + String.join("§8, §3", info.getAuthors())
            + " §awurde geladen§8." :
            "§cDas Spiel " + name + " §ckonnte nicht geladen werden§8!";
        sender.sendMessage(ArcadeMaster.PREFIX + msg);
      });
      return true;
    }
    if (args[0].equalsIgnoreCase("stop")) {
      this.moduleFactory.standBy();
      sender.sendMessage(ArcadeMaster.PREFIX + "§aDas aktuell laufende Spiel wurde gestoppt§8.");
      return true;
    }

    if (args[0].equalsIgnoreCase("info")) {
      ModuleInfo moduleInfo = moduleFactory.getActiveModuleInfo();
      if (moduleInfo == null) {
        sender.sendMessage(ArcadeMaster.PREFIX + "§cEs läuft derzeit kein Spiel§8!");
        return false;
      }
      sender.sendMessage(" ");
      sender.sendMessage("§a" + moduleInfo.getName() + "§8'§as stored information§8:");
      sender.sendMessage("§7UUID§8: §e" + moduleInfo.getUUID());
      sender.sendMessage("§7Author§8: §a" + String.join("§8,§a ", moduleInfo.getAuthors()));
      sender.sendMessage("§7Version§8: §e" + moduleInfo.getVersion());
      sender.sendMessage("§7Icon§8: §3" + moduleInfo.getDisplayMaterial());
      sender.sendMessage("§7Versteckt§8: " + (moduleInfo.isHidden() ? "§aJa" : "§cNein"));
      sender.sendMessage(" ");
      sender.sendMessage("§7Main§8: §e" + moduleInfo.getGameMasterPath());
      sender.sendMessage("§7Location§8: §e" + moduleInfo.getJarPath());
      sender.sendMessage(" ");
    }


    return false;
  }
}
