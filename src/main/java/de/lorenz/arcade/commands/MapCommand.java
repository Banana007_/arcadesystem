package de.lorenz.arcade.commands;

import de.lorenz.arcade.gui.Pager;
import de.lorenz.arcade.map.Map;
import de.lorenz.arcade.map.MapProvider;
import de.lorenz.arcade.map.PlayableMap;
import de.lorenz.arcade.map.Type;
import de.lorenz.arcade.system.ArcadeMaster;
import java.io.IOException;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class MapCommand {

  private final Pager<String> pager = new Pager<>(
      "§6/arcade map help §7Zeigt dir alle Map Befehle an§8.",
      "§6/arcade map list §7Zeigt dir den aktuellen Map Pool anu§8.",
      "§6/arcade map load §8[§6§aName§8] §7Lädt eine Map§8.",
      "§6/arcade map reset §8[§6§aName§8] §7Lädt eine Map neu§8.",
      "§6/arcade map tp §8[§6§aName§8] §7Teleportiert dich zu einer Map§8.",
      "§6/arcade map unload §8[§6§aName§8] §7Entlädt eine Map§8."
  );
  private final MapProvider mapProvider;

  public MapCommand(MapProvider mapProvider) {
    this.mapProvider = mapProvider;
  }

  public boolean onCommand(CommandSender sender, String[] args) {
    if (args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("help"))
        || (args.length == 2 && args[0].equalsIgnoreCase("help"))) {
      int page = 1;

      if (args.length == 2) {
        try {
          page = Integer.parseInt(args[1]);
        } catch (NumberFormatException ignored) {
          page = 1;
        }
      }

      sender.sendMessage(
          "§aÜbersicht der Befehle von Map Command §7(§e" + page + "§8/§e"
              + pager.getPages()
              + "§7)§8:");

      for (String i : pager.getPage(page)) {
        sender.sendMessage(i);
      }
      return true;
    }
    if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
      StringBuilder builder = new StringBuilder();
      builder.append("§aMap Pool §8(§e")
          .append(mapProvider.getPool().getMapList().size())
          .append("§8)")
          .append("\n");

      if (mapProvider.getPool().getMapList().isEmpty()) {
        builder.append("§cKeine Map gefunden :(");
        sender.sendMessage(builder.toString());
        return true;
      }

      for (Map map : mapProvider.getPool().getMapList()) {

        builder.append("§8[§a")
            .append(map.getTemplateName())
            .append("§8] §8[")
            .append(mapProvider.isActive(map) ? "§3ACTIVE" : "§6WAITING")
            .append("§8] §8[")
            .append(map.getType() == Type.NORMAL ? "§eNORMAL" : "§aGAME")
            .append("§8]\n");
      }
      sender.sendMessage(builder.toString());
      return true;
    }

    if (args.length == 2) {
      Map map = getMapProvider().getPool().getMap(args[1]);
      if (map == null) {
        sender.sendMessage(ArcadeMaster.PREFIX + "§cDie Welt wurde nicht gefunden§8!");
        return false;
      }

      if (args[0].equalsIgnoreCase("tp") || args[0].equalsIgnoreCase("teleport")) {
        if (getMapProvider().isActive(map)) {
          Player player = (Player) sender;
          Location location;

          if (map instanceof PlayableMap) {
            location = ((PlayableMap) map).getLocation();
          } else {
            location = map.getWorld().getSpawnLocation();
          }
          player.teleport(location);
          player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1F, 1F);
        } else {
          sender.sendMessage(ArcadeMaster.PREFIX + "§cDie Map ist nicht geladen§8!");
        }
      }

      if (args[0].equalsIgnoreCase("unload") || args[0].equalsIgnoreCase("remove")) {
        try {
          getMapProvider().deleteMap(map);
          sender
              .sendMessage(ArcadeMaster.PREFIX + "§aDie Map §e"
                + args[1] + " §awurde entfernt§8.");
        } catch (IOException e) {
          //TODO: LOGGER
          sender.sendMessage(ArcadeMaster.PREFIX + "§cDie Map §e" + args[1]
              + " §ckonnte nicht entfernt werden§8.");
          e.printStackTrace();
        }
      }

      if (args[0].equalsIgnoreCase("load")) {
        try {
          getMapProvider().createWorld(map);
        } catch (OverlappingFileLockException e) {
          sender.sendMessage(
              ArcadeMaster.PREFIX + "§cBitte warte einen Moment8...");
          return false;
        } catch (FileAlreadyExistsException e) {
          sender.sendMessage(ArcadeMaster.PREFIX
              + "§cAktuell kann das System nur eine Instance einer Map pro Session zulassen§8!");
          return false;
        }
        sender.sendMessage(
            ArcadeMaster.PREFIX + "§aDie Map §e" + map.getTemplateName()
              + " §awurde geladen§8.");
        return true;
      }

      if (args[0].equalsIgnoreCase("reset")
          || args[0].equalsIgnoreCase("reload")) {

        java.util.Map<Player, Location> cacheLocation = new HashMap();

        for (Player player : map.getWorld().getPlayers()) {
          Bukkit.broadcastMessage("Saving location of " + player.getName());
          cacheLocation.put(player, player.getLocation());
        }
        try {
          Bukkit.broadcastMessage("Reset the map....");
          getMapProvider().resetMap(map);

          for (Player player : cacheLocation.keySet()) {
            player.teleport(cacheLocation.get(player));
          }

          sender.sendMessage(
              ArcadeMaster.PREFIX + "§aDie Map §e" + map.getTemplateName()
                + " §awurde neu geladen§8.");
        } catch (IOException e) {
          sender.sendMessage(ArcadeMaster.PREFIX + "§cDie Map §e"
              + args[1] + " §ckonnte nicht neu geladen werden§8.");
        }
      }
    }
    return false;
  }

  private MapProvider getMapProvider() {
    return mapProvider;
  }
}
