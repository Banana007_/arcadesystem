package de.lorenz.arcade.gui;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * The pager can be used to distribute an array of an Object
 * into equally sized pages, these are output as sets. The {@link Pager#DEFAULT_WRAP}
 * specifies how many objects can be present in a page.
 *
 * @param <E> The Type wich should be wrapped
 */
public final class Pager<E> implements Iterable<E> {
    public static final int DEFAULT_WRAP = 5;
    private final E[] values;
    private final int wrapSize;

    /**
     * Create a Pager instance with an array of keys. The Wrap size is the {@link Pager#DEFAULT_WRAP}
     * @param keys the stored values in the Pager.
     */
    public Pager(E... keys) {
        this(DEFAULT_WRAP, keys);
    }

    /**
     * Create a Pager instance with a given Wrap size, wich declares how many items can hold a page.
     * @param wrapSize declares, how many items fits on a Page.
     * @param keys the stored values in the Pager.
     */
    @SafeVarargs
    public Pager(int wrapSize, E... keys) {
        this.values = keys;
        this.wrapSize = wrapSize;
    }

    /**
     * The items in Array will be split in {@link Set} with the size of the declared Wrap size. The last Set
     * is everytime same as the declared wrap size or smaller.
     *
     * @return a {@link Iterator} of {@link Set} of Objects.
     */
    public Iterator<Set<E>> pages() {
        return new Iterator<Set<E>>() {
            int current = 0;
            boolean filled = true;

            @Override
            public boolean hasNext() {
                return filled && current < values.length;
            }

            @Override
            public Set<E> next() {
                Iterator<E> currentPage = iterator(current, wrapSize);
                Set<E> content = new HashSet<>();

                filled = current + wrapSize < values.length;
                current += wrapSize;
                while (currentPage.hasNext()) {
                    content.add(currentPage.next());
                }
                return content;
            }
        };
    }

    /**
     * Get a page at the Pager.
     * @param page the page, wich you want, the first page starts with 0
     * @return A {@link Set} witch contains the stored item
     */
    public Set<E> getPage(int page) {

        if (page > getPages())
            page = getPages();


        Iterator<E> value = iterator(wrapSize * (page - 1), wrapSize);
        Set<E> col = new HashSet<>();

        while (value.hasNext()) {
            col.add(value.next());
        }
        return col;
    }

    public Iterator<E> iterator(int startIndex) {
        return iterator(startIndex, values.length);
    }

    public Iterator<E> iterator(int index, int limit) {
        return new Iterator<E>() {
            int current = index;
            int count = 0;
            final int max = index + limit;

            @Override
            public boolean hasNext() {
                return count < limit && index < values.length && current < max && current < values.length;
            }

            @Override
            public E next() {
                ++count;
                return values[current++];
            }
        };
    }

    @Override
    public Iterator<E> iterator() {
        return iterator(0);
    }

    /**
     * Get the total Pages in the Pager.
     * @return the number of pages, wich will be calculated from the wrap size and the length of the Array
     */
    public int getPages() {
        return (int) Math.ceil((double) values.length / wrapSize);
    }

    public int getSize() {
        return values.length;
    }
}
